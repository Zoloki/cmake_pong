/*!
 * @file      Ball.cpp
 * @brief     Represents the view showing the Pong game scene.
 * @copyright Peer Schneider
 */

#ifndef PONGFIELD_H
#define PONGFIELD_H

#include <QGraphicsView>

/**
 * The Pong game view. Apart from showing the pong scene the view has to handle the key presses
 * for controling the player rackets. Pressing or releasing will update according state variables
 * so that the keyboard state can be fetched from the scene. (This is possible because ther is
 * only one view for the scene, so the scene can use the first one it finds.)
 */
class PongField : public QGraphicsView
{
  Q_OBJECT
public:
  PongField(QWidget* parent = nullptr);

  bool p1UpPressed() const;
  bool p1DownPressed() const;
  bool p2UpPressed() const;
  bool p2DownPressed() const;

protected:
  void keyPressEvent(QKeyEvent* event) override;
  void keyReleaseEvent(QKeyEvent* event) override;

private:
  bool _p1UpPressed{};
  bool _p1DownPressed{};
  bool _p2UpPressed{};
  bool _p2DownPressed{};
};

#endif // PONGFIELD_H
