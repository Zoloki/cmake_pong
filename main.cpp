/*!
 * @file      %{Cpp:License:FileName}
 * @brief
 * @details
 * @copyright Scienta Omicron Technology GmbH
 * $Id$
 */
#include "Pong.h"

#include <QApplication>

int main(int argc, char *argv[])
{
  QApplication a(argc, argv);
  Pong w;
  w.show();
  return a.exec();
}
