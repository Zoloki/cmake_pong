/*!
 * @file      Ball.cpp
 * @brief     A graphics item representing a Racket in the Pong game.
 * @copyright Peer Schneider
 */

#ifndef RACKET_H
#define RACKET_H

#include <QGraphicsItem>

class Racket : public QGraphicsItem
{
public:
  Racket();

  QRectF boundingRect() const override;
  void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) override;

  auto width() const -> double;
  auto height() const -> double;
};

#endif // RACKET_H
