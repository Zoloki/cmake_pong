/*!
 * @file      Ball.cpp
 * @brief     Definitions we need throughout the application. Define here so we do not have to 
 *            include more heavy headers everywhere.
 * @copyright Peer Schneider
 */

#pragma once

enum class Players
{
  P1,
  P2
};
