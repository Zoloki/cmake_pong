/*!
 * @file      Ball.cpp
 * @brief     Represents the view showing the Pong game scene.
 * @copyright Peer Schneider
 */

#include "PongField.h"
#include <QKeyEvent>

PongField::PongField(QWidget* parent) : QGraphicsView(parent) {}

//----------------------------------------------------------------------------

bool PongField::p1UpPressed() const
{
  return _p1UpPressed;
}

//----------------------------------------------------------------------------

bool PongField::p1DownPressed() const
{
  return _p1DownPressed;
}

//----------------------------------------------------------------------------

bool PongField::p2UpPressed() const
{
  return _p2UpPressed;
}

//----------------------------------------------------------------------------

bool PongField::p2DownPressed() const
{
  return _p2DownPressed;
}

//----------------------------------------------------------------------------

void PongField::keyPressEvent(QKeyEvent* event)
{
  switch (event->key())
  {
    case Qt::Key_W:
      _p1UpPressed = true;
      event->accept();
      break;

    case Qt::Key_S:
      _p1DownPressed = true;
      event->accept();
      break;

    case Qt::Key_P:
      _p2UpPressed = true;
      event->accept();
      break;

    case Qt::Key_L:
      _p2DownPressed = true;
      event->accept();
      break;
  }
}

//----------------------------------------------------------------------------

void PongField::keyReleaseEvent(QKeyEvent* event)
{
  switch (event->key())
  {
    case Qt::Key_W:
      _p1UpPressed = false;
      break;

    case Qt::Key_S:
      _p1DownPressed = false;
      break;

    case Qt::Key_P:
      _p2UpPressed = false;
      break;

    case Qt::Key_L:
      _p2DownPressed = false;
      break;
  }
}
